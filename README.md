# SWORD dropbox
## A $15 OpenWRT based DIY disposable pen-test tool.

The article https://medium.com/@tomac/a-15-openwrt-based-diy-pen-test-dropbox-26a98a5fa5e5 says it best:

S.W.O.R.D is a web based UI for OpenWRT including common pentest tools: URLSnarf, Ettercap, tcpdump , nmap, etc.

## Prerequisites

`opkg update && opkg install ettercap-ng reaver tcpdump urlsnarf nmap mdk3 bash`

## How to install (currently)

Extract the files from the zip into the /www directory of your router

`chmod -R 655 /www/cgi-bin/`

Navigate to web GUI by going to http://ipaddress/SWORD